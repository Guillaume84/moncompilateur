			# This code was produced by the CERI Compiler
FormatString1:	.string "%llu\n"	 
	.data
	.align 8
a:	.quad 0
c:	.quad 0
z:	.quad 0
i:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $0
	pop a
FOR1 :
	push $0
	pop i
TO1 :
	push i
	push $3
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jbe Vrai2	# If below or equal
	push $0		# False
	jmp Suite2
Vrai2 :
	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rax
	cmpq $0, %rax
	je FINFOR1
DO1 :
BEGIN3 :
SWITCHCASE4 :
	push i
OF4 :
	pop %rcx
CASE1 :
	movq %rcx, %rax
	push $0
	pop %rbx
	cmpq %rax, %rbx
	jne CASE2
	push a
	push $5
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
	jmp CASEEND4
CASE2 :
	movq %rcx, %rax
	push $2
	pop %rbx
	cmpq %rax, %rbx
	jne CASE3
BEGIN5 :
	jmp WHILE6
DO6 :
	push a
	push $265
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
WHILE6 :
	push a
	push $1000
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jbe Vrai7	# If below or equal
	push $0		# False
	jmp Suite7
Vrai7 :
	push $0xFFFFFFFFFFFFFFFF		# True
Suite7:
	pop %rax
	cmpq $0, %rax
	jne DO6
FINDOWHILE6 :
END5 :
	jmp CASEEND4
CASE3 :
	movq %rcx, %rax
	push $3
	pop %rbx
	cmpq %rax, %rbx
	jne CASE4
BEGIN8 :
	push i
	push $115644
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop i
	push a
	push i
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
END8 :
	jmp CASEEND4
CASE4 :
	push a
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop a
CASEEND4 :
IF9 :
	push i
	push $3
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jae Vrai10	# If above or equal
	push $0		# False
	jmp Suite10
Vrai10 :
	push $0xFFFFFFFFFFFFFFFF		# True
Suite10:
	pop %rax
	movq $0, %rbx
	cmpq %rax, %rbx
	je ELSE9
THEN9 :
DISPLAY11 :
	push a
	pop %rdx
	movq $FormatString1, %rsi     # "%llu\n"
	movl $1, %edi
	movl $0, %eax
	call __printf_chk@PLT
	jmp FINSI9
ELSE9 :
	push a
	pop a
FINSI9 :
END3 :
	movq i, %rax
	addq $1, %rax
	movq %rax,i
	jmp TO1
FINFOR1 :
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
