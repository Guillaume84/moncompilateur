//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//  
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//  
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>
#include <map>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND ,WTFM};
enum TYPE  {INT, BOOL};
/*enum STATE {STATE};*/

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string

map <string, enum TYPE> TypeDeclaredVariables;

	
set<string> DeclaredVariables;
unsigned long TagNumber=0;
unsigned long TagNumber2=0;


bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := [DeclarationPart] StatementPart
// DeclarationPart := "[" Letter {"," Letter} "]"
// StatementPart := Statement {";" Statement} "."
// Statement := AssignementStatement
// AssignementStatement := Letter "=" Expression

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"
	
		
TYPE Identifier(void){
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	TYPE type;
	type = INT;
	return type;
}

TYPE Number(void){
	TYPE type;
	cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
	current=(TOKEN) lexer->yylex();
	type = INT;
	return type;
}

TYPE Expression(void);			// Called by Term() and calls Term()

TYPE Factor(void){
	TYPE type;
	if(current==RPARENT){
		current=(TOKEN) lexer->yylex();
		type = Expression();
		if(current!=LPARENT)
			Error("')' était attendu");		// ")" expected
		else
			current=(TOKEN) lexer->yylex();
	}
	else 
		if (current==NUMBER)
			type = Number();
	     	else
				if(current==ID)
					type = Identifier();
				else
					Error("'(' ou chiffre ou lettre attendue");
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPE Term(void){
	TYPE type;
	OPMUL mulop;
	type = Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		if (type != Factor())
			Error ("pas le meme type (dans la fonction Term)");
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(mulop){
			case AND:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# MUL"<<endl;	// store result
				break;
			case DIV:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
				cout << "\tpush %rax\t# DIV"<<endl;		// store result
				break;
			case MOD:
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator  
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPE SimpleExpression(void){
	TYPE type;
	OPADD adop;
	type = Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		if (type != Term())
			Error("pas le meme type (dans la fonction SimpleExpression)");
		cout << "\tpop %rbx"<<endl;	// get first operand
		cout << "\tpop %rax"<<endl;	// get second operand
		switch(adop){
			case OR:
				cout << "\taddq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				break;			
			case ADD:
				cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
				break;			
			case SUB:	
				cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
				break;
			default:
				Error("opérateur additif inconnu");
		}
		cout << "\tpush %rax"<<endl;			// store result
	}
	return type;
}

// DeclarationPart := "[" Ident {"," Ident} "]"
void DeclarationPart(void){
	if(current!=RBRACKET)
		Error("caractère '[' attendu");
	cout << "\t.data"<<endl;
	cout << "\t.align 8"<<endl;
	
	current=(TOKEN) lexer->yylex();
	if(current!=ID)
		Error("Un identificater était attendu");
	cout << lexer->YYText() << ":\t.quad 0"<<endl;
	DeclaredVariables.insert(lexer->YYText());
	TypeDeclaredVariables[(lexer->YYText())]=INT;
	current=(TOKEN) lexer->yylex();
	while(current==COMMA){
		current=(TOKEN) lexer->yylex();
		if(current!=ID)
			Error("Un identificateur était attendu");
		cout << lexer->YYText() << ":\t.quad 0"<<endl;
		DeclaredVariables.insert(lexer->YYText());
		TypeDeclaredVariables[(lexer->YYText())]=INT;
		current=(TOKEN) lexer->yylex();
	}
	if(current!=LBRACKET)
		Error("caractère ']' attendu");
	current=(TOKEN) lexer->yylex();
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="  
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPE Expression(void){
	TYPE type;
	OPREL oprel;
	type = SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		if (type != SimpleExpression())
			Error("pas le meme type (dans la fonction Expression)");
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<" :" << endl;
		cout << "\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;	
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOL;
	}
	return type;
}

void Statement(void);

// AssignementStatement := Identifier ":=" Expression
string AssignementStatement(void){
	TYPE type;
	TYPE type2;
	string variable;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type2 = TypeDeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type = Expression();
	cout << "\tpop "<<variable<<endl;
	return variable;
}



void IFStatement (void){
	TYPE type;
	unsigned long tagNum=++TagNumber;
	if (strcmp(lexer->YYText(), "IF") !=0)
		Error("IF attendu");
	cout << "IF"<< tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	type = Expression();
	if (type != BOOL)
	{
		Error("BOOL attendu");
	}
	cout << "\tpop %rax" << endl;
	cout << "\tmovq $0, %rbx" << endl;
	cout << "\tcmpq %rax, %rbx" << endl;
	cout << "\tje ELSE" << tagNum << endl;	
	if (strcmp(lexer->YYText(), "THEN") !=0)
		Error("Then attendu");
	//rajouter begin (pour plusieurs instructions
	cout << "THEN"<< tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	cout << "\tjmp FINSI" << tagNum << endl;
	cout << "ELSE"<< tagNum << " :" << endl;
	if (strcmp(lexer->YYText(), "ELSE") ==0)
	{
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	cout << "FINSI" << tagNum << " :" << endl;
}

void WHILEStatement (void){
	TYPE type;
	unsigned long tagNum=++TagNumber;
	if (strcmp(lexer->YYText(), "WHILE") !=0)
		Error("WHILE attendu");
	current=(TOKEN) lexer->yylex();
	cout << "WHILE" << tagNum << " :" << endl;
	type = Expression();
	if (type != BOOL)
	{
		Error("BOOL attendu");
	}
	cout <<"\tpop %rax" << endl;
	cout <<"\tcmpq $0, %rax" << endl;
	cout <<"\tje FINWHILE" <<tagNum << endl;
	if (strcmp(lexer->YYText(), "DO") !=0)
		Error("DO attendu");
	cout << "DO" << tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	cout << "\tjmp WHILE" << tagNum << endl;
	cout << "FINWHILE" << tagNum << " :" << endl;
	//rajouter begin (pour plusieurs instructions)
}

void FORStatement (void){
	TYPE type;
	unsigned long tagNum=++TagNumber;
	if (strcmp(lexer->YYText(), "FOR") !=0)
		Error("FOR attendu");
	cout << "FOR" << tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	string var = AssignementStatement();
	string txt;
	if (strcmp(lexer->YYText(), "TO") ==0)
		txt = "add";
	else if(strcmp(lexer->YYText(), "DOWNTO") ==0){
		txt = "sub";
		cout << "\t\t# Attention a ce que la valeur ne passe jamais au negatif, sinon cela cree une boucle infini" << endl;
	}
	else
		Error ("TO ou DOWNTO attendu");
	cout << "TO" << tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	type = Expression();
	cout <<"\tpop %rax" << endl;
	cout <<"\tcmpq $0, %rax" << endl;
	cout <<"\tje FINFOR" <<tagNum << endl;
	if (strcmp(lexer->YYText(), "DO") !=0)
		Error("DO attendu");
	cout << "DO" << tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	cout << "\tmovq " << var << ", %rax" << endl;
	cout << "\t" << txt << "q $1, %rax" << endl;
	cout << "\tmovq %rax," << var << endl;
	cout << "\tjmp TO" << tagNum << endl;
	cout << "FINFOR" << tagNum << " :" << endl;
	//rajouter begin (pour plusieurs instructions)
}

void BEGINStatement (void){
	unsigned long tagNum=++TagNumber;
	if (strcmp(lexer->YYText(), "BEGIN") !=0)
		Error("BEGIN attendu");
	cout << "BEGIN" << tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	while (strcmp(lexer->YYText(), "END") !=0)
	{
		Statement();
		if (strcmp(lexer->YYText(), ";") !=0)
				Error("; attendu");
		current=(TOKEN) lexer->yylex();
	}
	current=(TOKEN) lexer->yylex();
	cout << "END" << tagNum << " :" << endl;	
}

void DISPLAYStatement (void){
	TYPE type;
	unsigned long tagNum=++TagNumber;
	if (strcmp(lexer->YYText(), "DISPLAY") !=0)
		Error("DISPLAY attendu");
	cout << "DISPLAY"<< tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	type = SimpleExpression();
	if (type != INT)
		Error("INT attendu");
	cout << "\tpop %rdx" << endl;
	cout << "\tmovq $FormatString1, %rsi     # \"%llu\\n\"" << endl;
	cout << "\tmovl $1, %edi" << endl;
	cout << "\tmovl $0, %eax" << endl;
	cout << "\tcall __printf_chk@PLT" << endl;
}

void CASEStatement(void){
	TYPE type;
	TYPE type2;
	unsigned long tagNum=++TagNumber;
	unsigned long tagNum2=++TagNumber2;
	unsigned long tagNumSuiv=TagNumber2;
	if (strcmp(lexer->YYText(), "CASE") !=0)
		Error("CASE attendu");
	cout << "SWITCHCASE"<< tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	type = SimpleExpression();
	if (type != INT)
		Error("INT attendu");
	if (strcmp(lexer->YYText(), "OF") !=0)
		Error("OF attendu");
	cout << "OF"<< tagNum << " :" << endl;
	current=(TOKEN) lexer->yylex();
	cout << "\tpop %rcx" << endl;
	while (strcmp(lexer->YYText(), "ELSE") !=0)
	{
		tagNum2=TagNumber2;
		cout << "CASE"<< tagNum2 << " :" << endl;
		cout << "\tmovq %rcx, %rax" << endl;
		type2 = SimpleExpression();
		if (type2 != type)
			Error("type non identique (fonction CASE)");
		if (strcmp(lexer->YYText(), ":") !=0)
			Error(": attendu");
		tagNumSuiv=++TagNumber2;
		cout << "\tpop %rbx" << endl;
		cout << "\tcmpq %rax, %rbx" << endl;
		cout << "\tjne CASE" << tagNumSuiv << endl;
		current=(TOKEN) lexer->yylex();
		Statement();
		cout << "\tjmp CASEEND" << tagNum << endl;
	}
	cout << "CASE"<< tagNumSuiv << " :" << endl;
	current=(TOKEN) lexer->yylex();
	Statement();
	cout << "CASEEND"<< tagNum << " :" << endl;
}

void DOWHILEStatement(void){
	TYPE type;
	unsigned long tagNum=++TagNumber;
	if (strcmp(lexer->YYText(), "DO") !=0)
		Error("DO attendu");
	current=(TOKEN) lexer->yylex();
	cout << "\tjmp WHILE" << tagNum << endl;
	cout << "DO" << tagNum << " :" << endl;
	Statement();
	//rajouter begin (pour plusieurs instructions)
	if (strcmp(lexer->YYText(), "WHILE") !=0)
		Error("WHILE attendu");
	current=(TOKEN) lexer->yylex();
	cout << "WHILE" << tagNum << " :" << endl;
	type = Expression();
	if (type != BOOL)
	{
		Error("BOOL attendu");
	}
	cout <<"\tpop %rax" << endl;
	cout <<"\tcmpq $0, %rax" << endl;
	cout <<"\tjne DO" <<tagNum << endl;
	cout <<"FINDOWHILE" <<tagNum << " :" << endl;
}

// Statement := AssignementStatement
void Statement(void){
	if (strcmp(lexer->YYText(), "IF") == 0)
		IFStatement();
	else if (strcmp(lexer->YYText(), "FOR") == 0)
		FORStatement();
	else if (strcmp(lexer->YYText(), "WHILE") == 0)
		WHILEStatement();
	else if (strcmp(lexer->YYText(), "BEGIN") == 0)
		BEGINStatement();
	else if (strcmp(lexer->YYText(), "DISPLAY") == 0)
		DISPLAYStatement();
	else if (strcmp(lexer->YYText(), "CASE") == 0)
		CASEStatement();
	else if (strcmp(lexer->YYText(), "DO") == 0)
		DOWHILEStatement();
	else
		AssignementStatement();
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

// Program := [DeclarationPart] StatementPart
void Program(void){
	if(current==RBRACKET)
		DeclarationPart();
	StatementPart();	
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "FormatString1:\t.string \"%llu\\n\"\t " << endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
		
			





